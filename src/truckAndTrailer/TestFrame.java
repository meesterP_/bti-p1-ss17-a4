package truckAndTrailer;

public class TestFrame {

    public static void main(String[] args) {
        TestFrame tf = new TestFrame();
        tf.testToString();
        System.out.println("--------------------");
        tf.testAttachAtTheEnd();
        System.out.println("--------------------");
        tf.testDetach();
        System.out.println("--------------------");
        //tf.testGetHeaviestTrailer();
    }

    public void testToString() {
        Truck truck = new Truck();
        Trailer t1 = new Trailer(5, "A");
        Trailer t2 = new Trailer(2, "B");
        Trailer t3 = new Trailer(4, "C");
        System.out.println(truck);

        truck.setTrailer(t1);
        t1.attach(t2);
        t2.attach(t3);

        System.out.println(truck);
    }

    public void testAttachAtTheEnd() {
        Truck truck = new Truck();
        Trailer t1 = new Trailer(5, "A");
        Trailer t2 = new Trailer(2, "B");
        Trailer t3 = new Trailer(4, "C");
        Trailer t4 = new Trailer(7, "D");
        truck.attachAtTheEnd(t1);
        truck.attachAtTheEnd(t2);
        truck.attachAtTheEnd(t3);
        truck.attachAtTheEnd(t4);
        System.out.println(truck);
    }

    public void testDetach() {
        Truck truck = new Truck();
        Trailer t1 = new Trailer(5, "A");
        Trailer t2 = new Trailer(2, "B");
        Trailer t3 = new Trailer(4, "C");
        Trailer t4 = new Trailer(7, "D");

        truck.setTrailer(t1);
        t1.attach(t2);
        t2.attach(t3);
        t3.attach(t4);

        System.out.println(truck);

        truck.detach(t3);

        System.out.println(truck);

        truck.detach(t1);

        System.out.println(truck);
    }

    public void testGetHeaviestTrailer() {
        Truck truck = new Truck();
        Trailer t1 = new Trailer(5, "A");
        Trailer t2 = new Trailer(2, "B");
        Trailer t3 = new Trailer(4, "C");
        Trailer t4 = new Trailer(7, "D");

        truck.setTrailer(t1);
        t1.attach(t2);
        t2.attach(t3);
        t3.attach(t4);

        System.out.println(truck.getHeaviestTrailer());
    }
}
