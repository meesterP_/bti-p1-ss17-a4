package truckAndTrailer;

public class Trailer {
    private Trailer trailer = null;
    private int load = 0;
    private String id = "";

    public Trailer(int load, String id) {
        this.load = load;
        this.id = id;
    }

    public int getLoad() {
        return load;
    }

    public String toString() {
        return " - [" + id + ", " + load + "t]";
    }

    public void attach(Trailer trailer) {
        this.trailer = trailer;
    }

    public Trailer getTrailer() {
        return trailer;
    }
}
