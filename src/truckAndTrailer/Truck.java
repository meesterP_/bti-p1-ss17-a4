package truckAndTrailer;

public class Truck {
    private Trailer trailer = null;

    public Truck() {

    }

    @Override
    public String toString() {
        String roadTrain = "[LKW]";
        Trailer nextTrailer = trailer;
        while (nextTrailer != null) {
            roadTrain += nextTrailer.toString();
            nextTrailer = nextTrailer.getTrailer();
        }
        return roadTrain;
    }

    public void setTrailer(Trailer trailer) {
        this.trailer = trailer;
    }

    public void attachAtTheEnd(Trailer trailer) {
        Trailer nextTrailer = this.trailer;
        if (nextTrailer != null) {
            while (nextTrailer.getTrailer() != null) {
                nextTrailer = nextTrailer.getTrailer();
            }
            nextTrailer.attach(trailer);       
        } else {
            this.setTrailer(trailer);
        }
    }

    public void detach(Trailer trailer) {
        Trailer nextTrailer = this.trailer;
        if (nextTrailer != null) {
            while (!nextTrailer.getTrailer().equals(trailer)) {
                nextTrailer = nextTrailer.getTrailer();
            }
            nextTrailer.attach(nextTrailer.getTrailer().getTrailer());          
        } else if (this.trailer.equals(trailer)) {
            this.trailer = this.trailer.getTrailer();
        }
    }

    public Trailer getHeaviestTrailer() {
        Trailer heaviestTrailer = trailer;
        Trailer nextTrailer = trailer;
        while (nextTrailer.getTrailer().getTrailer() != null) {
            if (heaviestTrailer.getLoad() < nextTrailer.getTrailer().getLoad()) {
                heaviestTrailer = nextTrailer;
            }
            nextTrailer = nextTrailer.getTrailer();
        }
        return heaviestTrailer;
    }
}
